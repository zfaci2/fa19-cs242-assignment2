import urllib.request as request
from bs4 import BeautifulSoup
import re
import logging


def scrape_actor(url):
    logging.basicConfig(filename="./logs/ActorScraper_logs.log")

    page = request.urlopen(url).read()
    page = BeautifulSoup(page, "html.parser")
    page.prettify()

    vcard = page.find("table", {"class": "infobox biography vcard"})
    if vcard is None:
        logging.error("Actor vcard not found. Stopping search")
        return

    actor_name = ""
    found_name = vcard.find("div", {"class": "fn"})
    if found_name is None:
        logging.warning("The actor's name could not be found")
    else:
        logging.info("The actor's name was found")
        actor_name = found_name.get_text()

    actor_age = -1
    found_age = vcard.find("span", {"class": "noprint ForceAgeToShow"})
    if found_age is None:
        logging.warning("The actor's age could not be found")
    else:
        logging.info("The actor's age was found")
        actor_age = re.sub("[^0-9]", "", found_age.contents[0])

    actor_movies = []
    filmography = page.find("span", {"id": "Filmography"})
    if filmography is None:
        logging.warning("The actor's films could not be found")
    else:
        logging.info("The actor's films were found")
        film_list = filmography.find_next("ul")
        film_table = filmography.find_next("tbody")

        for film in film_list.findAll("i"):
            movie_item = film.find("a")
            if movie_item is None:
                continue
            movie_name = movie_item.get_text()
            movie_link = "https://en.wikipedia.org" + movie_item.get("href")
            actor_movies.append([movie_name, movie_link])

        if len(film_list) == 0:
            for film in film_table.findAll("td"):
                movie_item = film.find("a")
                if movie_item is None:
                    continue
                movie_name = movie_item.get_text()
                movie_link = "https://en.wikipedia.org" + movie_item.get("href")
                actor_movies.append([movie_name, movie_link])

    actor_json = {
        "url": url,
        "name": actor_name,
        "age": actor_age,
        "movies": actor_movies
    }

    return actor_json


#print(scrape_actor("https://en.wikipedia.org/wiki/Piper_Perabo"))
