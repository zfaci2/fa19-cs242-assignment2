import ActorScraper
import MovieScraper

movie_list = []
actor_list = []

catalyst_movie_url = "https://en.wikipedia.org/wiki/Ted_2"
catalyst_json = MovieScraper.scrape_movie(catalyst_movie_url)
movie_list.append(catalyst_json)

while len(movie_list) < 125 or len(actor_list) < 250:
    if len(actor_list) < 125:
        for movie in movie_list:
            if "cast" in movie:
                actors = movie["cast"]
            else:
                continue
            for actor in actors:
                if not any(actor[0] == i["name"] for i in actor_list):
                    print(actor)
                    actor_details = ActorScraper.scrape_actor(actor[1])
                    actor_list.append(actor_details)

    if len(movie_list) < 125:
        for actor in actor_list:
            movies = actor["movies"]
            for movie in movies:
                if not any(movie[0] == i["name"] for i in movie_list):
                    print(movie)
                    movie_details = MovieScraper.scrape_movie(movie[1])
                    movie_list.append(actor_details)

print(len(actor_list))
print(len(movie_list))