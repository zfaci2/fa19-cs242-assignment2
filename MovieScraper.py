from bs4 import BeautifulSoup
import urllib.request as request
import logging


def scrape_movie(url):
    logging.basicConfig(filename="./logs/MovieScraper_logs.log")

    page = request.urlopen(url).read()
    page = BeautifulSoup(page, "html.parser")
    page.prettify()

    infobox = page.find("table", {"class": "infobox vevent"})
    if infobox is None:
        logging.error("Movie info box not found. Stopping search.")
        return

    movie_name = ""
    found_name = infobox.find("th", {"class": "summary"})
    if found_name is None:
        logging.warning("Movie name could not be found")
    else:
        logging.info("Movie name was found")
        movie_name = found_name.text

    movie_gross = -1
    found_gross = infobox.find(text="Box office")
    if found_gross is None:
        logging.warning("Movie gross could not be found")
    else:
        logging.info("Movie gross was found")
        movie_gross = found_gross.find_next("td").contents[0]

    movie_cast = []
    found_cast = infobox.find(text="Starring")
    if found_cast is None:
        logging.warning("Movie cast could not be found")
    else:
        logging.info("Movie cast was found")
        found_cast = found_cast.find_next("ul")
        cast_a_list = found_cast.findAll("a")

        for cast_member in cast_a_list:
            cast_member_name = cast_member.get_text()
            cast_member_link = "https://en.wikipedia.org" + cast_member.get("href")
            movie_cast.append([cast_member_name, cast_member_link])

    movies_json = {
        "url": url,
        "name": movie_name,
        "gross": movie_gross,
        "cast": movie_cast
    }

    return movies_json


#print(scrape_movie("https://en.wikipedia.org/wiki/The_Dark_Knight_(film)"))
